package com.micro.fast.security.app.master;

import lombok.Getter;
import lombok.Setter;

/**
 * aouth2属性配置
 * @author lsy
 */
@Getter
@Setter
public class OAuth2Properties {

    private OAuth2ClientProperties[] clients = new OAuth2ClientProperties[]{};

    private String jwtSigningKey = "msjwt";

    private String storeType = "jwt";
}
